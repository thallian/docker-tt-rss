FROM registry.gitlab.com/thallian/docker-php7-fpm:master

ENV FPMUSER nginx
ENV FPMGROUP nginx
ENV LC_ALL en_us.UTF-8

ENV VERSION 17.4

RUN apk --no-cache add \
    libressl \
    nginx \
    postgresql-client \
    php7 \
    php7-json \
    php7-dom \
    php7-mbstring \
    php7-fileinfo \
    php7-curl \
    php7-posix \
    php7-gd \
    php7-ldap \
    php7-pdo_pgsql \
    php7-pgsql \
    php7-opcache \
    php7-iconv \
    php7-intl \
    php7-session

RUN mkdir /var/lib/tt-rss
RUN wget -qO- https://git.tt-rss.org/git/tt-rss/archive/${VERSION}.tar.gz | tar -xz -C /var/lib/tt-rss --strip 1

RUN mkdir /var/lib/tt-rss/plugins/auth_ldap
RUN wget -qO- https://github.com/tsmgeek/ttrss-auth-ldap/archive/master.tar.gz | tar -xz -C /var/lib/tt-rss/plugins/auth_ldap --strip 3 ttrss-auth-ldap-master/plugins/auth_ldap

RUN chown -R nginx:nginx /var/lib/tt-rss
RUN mkdir /run/nginx
RUN rm /etc/nginx/conf.d/default.conf

RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log

ADD /rootfs /
