[Tiny Tiny RSS](https://tt-rss.org/) server with ldap authentication

# Environment Variables
## DB_HOST
Database host.

## DB_USER
Database user.

## DB_NAME
Database name.

## DB_PASSWORD
Password for database user.

## DB_PORT
- default: 5432

Database port.

## TTRSS_URL
Full URL of the tt-rss installation.

## SMTP_FROM_NAME
- default: Tiny Tiny RSS

Name from which mails are sent from.

## SMTP_FROM_ADDRESS
Email Address from which mails are sent from.

## DIGEST_SUBJECT
- default: [tt-rss] New headlines for last 24 hours

Subject in digest mails.

## SMTP_SERVER
Hostname and port for the used smtp relay (for example `mail.example.com:587`).

## SMTP_LOGIN
User to authenticate agains the smtp relay.

## SMTP_PASSWORD
Password to authenticate agains the smtp relay.

## SMTP_SECURE
- default: tls

How to connect to the smtp server.

One of:
- empty string
- ssl
- tls

## LDAP_SERVER
Connection string for the ldap server.

## LDAP_USE_STARTTLS
- default: TRUE

Whether to use STARTTLS when connecting to the ldap server.

## LDAP_ALLOW_UNTRUSTED_CERT
- default: FALSE

Whether to accept untrusted certificates on an encrypted connection.

## LDAP_BASE_DN
Base DN on which to connect.

## LDAP_USER_FILTER
Filter to find users which have access. `???` gets replaced with the username.

## LDAP_USER
User to connect to the ldap server.

## LDAP_PASSWORD
Password for the ldap user.

## LDAP_LOGIN_ATTRIBUTE
Login attribute in ldap.

# Ports
- 80
